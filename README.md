# ML Models

This is project for product code

## About
TODO

## Environment preparation (local)

1. Configure IDE, poetry and pre-commit (optional)
   * IDE: [Lecture from course](https://youtu.be/eBaUoN2ExHU)
   * Poetry: https://python-poetry.org/docs/
   * pre-commit: https://pre-commit.com

2. Git clone

3. Configure local python and install python packages
    You can use any python version
```bash
pyenv local 3.10
poetry env use 3.10
poetry install
pre-commit install
```
See all additional settings in `pyproject.toml`

4. (optional) Install jupyter kernel
```bash
poetry run python -m ipykernel install --user --name ods_mlops_course_venv
```
