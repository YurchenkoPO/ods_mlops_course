# Contributing to CONTRIBUTING.md


## Linters and formatters we use

* ruff
* mypy

## How to use

1. Locally
    ```bash
    ruff check
    ruff format
    mypy src
    ```
2. Pre-commit
    - Firstly add/stage all the changes
    - run ```pre-commit```
